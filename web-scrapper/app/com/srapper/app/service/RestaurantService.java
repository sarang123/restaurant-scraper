package com.srapper.app.service;

import java.io.IOException;
import java.util.Collection;

import org.jsoup.select.Elements;

import com.srapper.infra.model.RestataurantModel;

public interface RestaurantService {

	public void saveAll(Collection<RestataurantModel> restataurantModels );
	
	public String getTextFromElements(Elements elements);
	
	public String getLinkFromanchorElements(Elements elements);
	public int getPageNOFromElements(Elements elements);
	
	public Collection<RestataurantModel> getModelSFromURL(String url ,int count)throws IOException;
	
	public Collection<RestataurantModel> getAll();
}
