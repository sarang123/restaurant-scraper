package com.srapper.app.service.Impl;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Collection;

import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;
import org.jsoup.nodes.Element;
import org.jsoup.select.Elements;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.srapper.app.domain.entities.RestaurantInfoEntity;
import com.srapper.app.service.RestaurantService;
import com.srapper.infra.model.RestataurantModel;
import com.srapper.infra.repository.RestaurantInfoRepository;
import com.srapper.util.Constant;

@Component
public class RestaurantServiceImpl implements RestaurantService {

	@Autowired
	private RestaurantInfoRepository  restaurantInfoRepository;

	

	@Override
	public String getTextFromElements(Elements elements) {
		if (elements != null && elements.size() > 0) {
			return elements.get(0).text();
		}
		else
			return null;
	}

	@Override
	public String getLinkFromanchorElements(Elements elements) {
		if (elements != null && elements.size() > 0) {
			return elements.get(0).attr(Constant.Selecter.HREF);
		}
		else
			return null;
	}

	@Override
	public int getPageNOFromElements(Elements elements) {
		if (elements != null && elements.size() > 0) {
			String pageNo = elements.get(0).text().replace(Constant.TextToReplace.PAGE_1_OF, "");
			try{
				return Integer.parseInt(pageNo);
			}catch(NumberFormatException e){

				return 1;

			}
		}
		else
			return 0;
	}

	@Override
	public Collection<RestataurantModel> getModelSFromURL(String url,int count) throws IOException {

		int noOfPage = 1;

		ArrayList<RestataurantModel> restarants = new  ArrayList<>();

		for(int i=0;i<count;i++){

			String fullUrl = url+Constant.TextToReplace.WHICH_PAGE+(noOfPage++);
			Document doc = Jsoup.connect(fullUrl).timeout(88888).get();
			Element link = doc.getElementById(Constant.Selecter.SEARCH_LIST_ID);
			Elements lks = link.children();

			for (Element element : lks) {

				RestataurantModel restarant = new RestataurantModel();

				restarant.setName(getTextFromElements(element.select(Constant.Selecter.NAME)));
				restarant.setLink(getLinkFromanchorElements(element.select(Constant.Selecter.NAME)));
				restarant.setAddress(getTextFromElements(element.select(Constant.Selecter.ADDRESS)));
				restarant.setArea(getTextFromElements(element.select(Constant.Selecter.AREA)));
				restarant.setCuisine(getTextFromElements(element.select(Constant.Selecter.CUISINE)));
				restarant.setFeaturedInCollections(getTextFromElements(element.select(Constant.Selecter.FEATURED_IN_COLLECTION)));
				restarant.setOffers(getTextFromElements(element.select(Constant.Selecter.OFFERS)));
				restarant.setPriceWithNopeople(getTextFromElements(element.select(Constant.Selecter.SEATES_AND_PRICE)));
				restarant.setRating(getTextFromElements(element.select(Constant.Selecter.RATING)));
				restarant.setTodaySpecial(getTextFromElements(element.select(Constant.Selecter.TODAY_SPECIAL)));
				restarant.setHours(getTextFromElements(element.select(Constant.Selecter.HOURES)));
				restarant.setVotes(getTextFromElements(element.select(Constant.Selecter.NO_OF_VOTES)));
				restarants.add(restarant);
			}

		}
		return restarants;
	}

	@Override
	public void saveAll(Collection<RestataurantModel> restataurantModels) {
		restaurantInfoRepository.save(RestataurantModel.createEntities(restataurantModels));
		
	}

	@Override
	public Collection<RestataurantModel> getAll() {
		// TODO Auto-generated method stub
		return RestataurantModel.createModels((Collection<RestaurantInfoEntity>)restaurantInfoRepository.findAll());
	}

	

}
