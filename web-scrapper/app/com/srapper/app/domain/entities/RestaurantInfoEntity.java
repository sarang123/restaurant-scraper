package com.srapper.app.domain.entities;

import java.io.Serializable;

import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.mapping.Document;
import org.springframework.data.mongodb.core.mapping.Field;


@Document(collection = RestaurantInfoEntity.COLLECTION_NAME)
public class RestaurantInfoEntity  implements Serializable{
	
	public static final String COLLECTION_NAME = "restaurant_info";
	
	
	public static final String SEARCH_URL = "search_url";
	public static final String NAME = "name";
	public static final String LINK = "link";
	public static final String AREA = "area";
	public static final String RATING = "rating";
	public static final String NO_OF_VOTES = "no_of_votes";
	public static final String ADDRESS = "address";
	public static final String  CUISINE = "cuisine";
	public static final String HOURS = "hours";
	public static final String NO_OF_SEATES_AND_PRICE = "no_of_seates_and_price";
	public static final String  FEATURED_IN_COLLECTION = "featured_in_collections";
	public static final String TODAY_SPECIAL = "today_special";
	public static final String OFFERS = "offers";
	
	@Id
    String id;	
	
	 @Field(NAME)
	String name;
	
	 @Field(LINK)
	 String link;
	 
	 @Field(AREA)
	 String area;
	 
	 @Field(RATING)
	 String rating;
	
	 @Field(NO_OF_VOTES)
	 String votes;
	
	 @Field(ADDRESS)
	 String address;
    
	 @Field(CUISINE)
	 String cuisine;
    
	 @Field(HOURS)
	 String hours;
    
	 @Field(NO_OF_SEATES_AND_PRICE)
	 String priceWithNopeople;
   
	 @Field(FEATURED_IN_COLLECTION)
	 String FeaturedInCollections;
	 
	 @Field(OFFERS)
	 String offers;

    
	 @Field(TODAY_SPECIAL)
	 String todaySpecial;
    
    
    
	public String getId() {
		return id;
	}
	public void setId(String id) {
		this.id = id;
	}
	public String getTodaySpecial() {
		return todaySpecial;
	}
	public void setTodaySpecial(String todaySpecial) {
		this.todaySpecial = todaySpecial;
	}
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public String getLink() {
		return link;
	}
	public void setLink(String link) {
		this.link = link;
	}
	public String getArea() {
		return area;
	}
	public void setArea(String area) {
		this.area = area;
	}
	public String getRating() {
		return rating;
	}
	public void setRating(String rating) {
		this.rating = rating;
	}
	public String getVotes() {
		return votes;
	}
	public void setVotes(String votes) {
		this.votes = votes;
	}
	public String getAddress() {
		return address;
	}
	public void setAddress(String address) {
		this.address = address;
	}
	public String getCuisine() {
		return cuisine;
	}
	public void setCuisine(String cuisine) {
		this.cuisine = cuisine;
	}
	public String getHours() {
		return hours;
	}
	public void setHours(String hours) {
		this.hours = hours;
	}
	public String getPriceWithNopeople() {
		return priceWithNopeople;
	}
	public void setPriceWithNopeople(String priceWithNopeople) {
		this.priceWithNopeople = priceWithNopeople;
	}
	public String getFeaturedInCollections() {
		return FeaturedInCollections;
	}
	public void setFeaturedInCollections(String featuredInCollections) {
		FeaturedInCollections = featuredInCollections;
	}
	public String getOffers() {
		return offers;
	}
	public void setOffers(String offers) {
		this.offers = offers;
	}
	
	
	@Override
	public String toString() {
		return "RestaurantInfo [name=" + name + ", link=" + link + ", area=" + area + ", rating=" + rating + ", votes="
				+ votes + ", address=" + address + ", cuisine=" + cuisine + ", hours=" + hours + ", priceWithNopeople="
				+ priceWithNopeople + ", FeaturedInCollections=" + FeaturedInCollections + ", offers=" + offers
				+ "]";
	}
    
    
}
