package com.srapper.controllers;

import play.Play;
import play.mvc.*;
import java.io.IOException;




/**
 * The main set of web services.
 */


public class Application extends Controller {
	
	
	public static Result index() throws IOException {
		
			 return ok(Play.application().classloader().getResourceAsStream("public/index.html")).as("text/html");
		 
	
	}
	
	
	
    
}
