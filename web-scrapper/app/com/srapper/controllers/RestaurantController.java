package com.srapper.controllers;



import java.io.IOException;
import java.util.Collection;

import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;
import org.jsoup.select.Elements;

import com.srapper.app.service.RestaurantService;
import com.srapper.infra.model.RestataurantModel;
import com.srapper.infra.spring.SpringContext;
import com.srapper.util.Constant;

import play.libs.Json;
import play.mvc.Controller;
import play.mvc.Result;

public class RestaurantController extends Controller {
	
	private static RestaurantService restaurantService = SpringContext.getApplicationContext().getBean(RestaurantService.class); 
	
	public static Result getRestaurantsFromUrl(String searchUrl){
    
		
		try {
			Document startDoc = Jsoup.connect(searchUrl).timeout(88888).get();
			Elements noOfRestarents =	startDoc.select(Constant.Selecter.NO_OF_PAGES);
			int noOfPages = restaurantService.getPageNOFromElements(noOfRestarents);
			
			Collection<RestataurantModel> restataurantModels  =  restaurantService.getModelSFromURL(searchUrl, noOfPages);
			
			restaurantService.saveAll(restataurantModels);
			
			return ok(Json.toJson(restataurantModels));
			
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
			return badRequest("Reading problem from url");
		}
		
	}
	
      public static Result getRestaurantsFromDataBase(){
    
    	  return ok(Json.toJson(restaurantService.getAll()));
		
		
	}
}