package com.srapper.util;


public class Constant {

	public static class Selecter{
		public static final String NAME = "a[class=result-title]";
		public static final String AREA = "a[class=search-page-text zblack search_result_subzone]";
		public static final String RATING = "div[class=tooltip rating-for-47221 res-rating-nf right level-7]";
		public static final String NO_OF_VOTES = "span[class=rating-votes-div-47221]";
		public static final String ADDRESS = "div[class=search-result-address zdark]";
		public static final String  CUISINE = "span[class=search-grid-right-text]";
		public static final String HOURES = "span[class=left search-grid-right-text]";
		public static final String SEATES_AND_PRICE = "div[class=res-cost clearfix]";
		public static final String  FEATURED_IN_COLLECTION = "div[class=srp-collections]";
		public static final String TODAY_SPECIAL = "div[class=res-events clearfix]";
		public static final String OFFERS = "div[class=res-offers clearfix]";
		public static final String NO_OF_PAGES = "div[class=col-l-3 mtop0 alpha tmargin pagination-number]";
		public static final String SEARCH_LIST_ID = "orig-search-list";
		public static final String HREF = "href";
	}
	
	public static class TextToReplace{
		public static final String PAGE_1_OF = "Page 1 of ";
		public static final String WHICH_PAGE = "?page=";
		
	}
}
