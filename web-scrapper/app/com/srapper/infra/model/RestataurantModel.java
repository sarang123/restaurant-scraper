package com.srapper.infra.model;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.Collection;

import org.bson.types.ObjectId;

import com.srapper.app.domain.entities.RestaurantInfoEntity;

public class RestataurantModel implements Serializable {

	String id;
	String name;
	String link;
	String area;
	String rating;
	String votes;
	String address;
    String cuisine;
    String hours;
    String priceWithNopeople;
    String FeaturedInCollections;
    String offers;
    String todaySpecial;
	
    
    
 public static Collection<RestaurantInfoEntity> createEntities (Collection<RestataurantModel> restataurantModels){
		if(restataurantModels == null) return new ArrayList<>();
		
		Collection<RestaurantInfoEntity> RestaurantInfoEntities = new ArrayList<>();

		for (RestataurantModel restataurantModel : restataurantModels) {
    		RestaurantInfoEntities.add(createEntity(restataurantModel));
		}
    	
    	return RestaurantInfoEntities;
    }
 
 
 public static Collection<RestataurantModel> createModels (Collection<RestaurantInfoEntity> restaurantInfoEntities){
	 if(restaurantInfoEntities == null) return new ArrayList<>();
 	
	 Collection<RestataurantModel> restataurantModels = new ArrayList<>();
	 
	for (RestaurantInfoEntity restaurantInfoEntity : restaurantInfoEntities) {
		restataurantModels.add(createModel(restaurantInfoEntity));
	}
	 
 	return restataurantModels;
 	
 }
    
    
    
	public static RestaurantInfoEntity createEntity(RestataurantModel restataurantModel) {
	
		if (restataurantModel == null)return null;
		
		RestaurantInfoEntity restaurantInfoEntity = new RestaurantInfoEntity();
		
		restaurantInfoEntity.setId(restataurantModel.getId() == null ? new ObjectId().toString() : restataurantModel.getId());
		restaurantInfoEntity.setName(restataurantModel.getName());
		restaurantInfoEntity.setAddress(restataurantModel.getAddress());
		restaurantInfoEntity.setArea(restataurantModel.getArea());
		restaurantInfoEntity.setCuisine(restataurantModel.getCuisine());
		restaurantInfoEntity.setFeaturedInCollections(restataurantModel.getFeaturedInCollections());
		restaurantInfoEntity.setLink(restataurantModel.getLink());
		restaurantInfoEntity.setOffers(restataurantModel.getOffers());
		restaurantInfoEntity.setPriceWithNopeople(restataurantModel.getPriceWithNopeople());
		restaurantInfoEntity.setRating(restataurantModel.getRating());
		restaurantInfoEntity.setTodaySpecial(restataurantModel.getTodaySpecial());
		restaurantInfoEntity.setHours(restataurantModel.getHours());
		restaurantInfoEntity.setVotes(restataurantModel.getVotes());
		
		return restaurantInfoEntity;

	}
    
	public static RestataurantModel createModel(RestaurantInfoEntity restaurantInfoEntity) {
		if (restaurantInfoEntity == null)
			return null;

		RestataurantModel restataurantModel = new RestataurantModel();

		restataurantModel.setId(restaurantInfoEntity.getId());
		restataurantModel.setName(restaurantInfoEntity.getName());
		restataurantModel.setAddress(restaurantInfoEntity.getAddress());
		restataurantModel.setArea(restaurantInfoEntity.getArea());
		restataurantModel.setCuisine(restaurantInfoEntity.getCuisine());
		restataurantModel.setFeaturedInCollections(restaurantInfoEntity.getFeaturedInCollections());
		restataurantModel.setLink(restaurantInfoEntity.getLink());
		restataurantModel.setOffers(restaurantInfoEntity.getOffers());
		restataurantModel.setPriceWithNopeople(restaurantInfoEntity.getPriceWithNopeople());
		restataurantModel.setRating(restaurantInfoEntity.getRating());
		restataurantModel.setTodaySpecial(restaurantInfoEntity.getTodaySpecial());
		restataurantModel.setHours(restaurantInfoEntity.getHours());
		restataurantModel.setVotes(restaurantInfoEntity.getVotes());

		return restataurantModel;

	}
    
    
    
    
    
    
    public String getId() {
		return id;
	}
	public void setId(String id) {
		this.id = id;
	}
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public String getLink() {
		return link;
	}
	public void setLink(String link) {
		this.link = link;
	}
	public String getArea() {
		return area;
	}
	public void setArea(String area) {
		this.area = area;
	}
	public String getRating() {
		return rating;
	}
	public void setRating(String rating) {
		this.rating = rating;
	}
	public String getVotes() {
		return votes;
	}
	public void setVotes(String votes) {
		this.votes = votes;
	}
	public String getAddress() {
		return address;
	}
	public void setAddress(String address) {
		this.address = address;
	}
	public String getCuisine() {
		return cuisine;
	}
	public void setCuisine(String cuisine) {
		this.cuisine = cuisine;
	}
	public String getHours() {
		return hours;
	}
	public void setHours(String hours) {
		this.hours = hours;
	}
	public String getPriceWithNopeople() {
		return priceWithNopeople;
	}
	public void setPriceWithNopeople(String priceWithNopeople) {
		this.priceWithNopeople = priceWithNopeople;
	}
	public String getFeaturedInCollections() {
		return FeaturedInCollections;
	}
	public void setFeaturedInCollections(String featuredInCollections) {
		FeaturedInCollections = featuredInCollections;
	}
	public String getOffers() {
		return offers;
	}
	public void setOffers(String offers) {
		this.offers = offers;
	}
	
	public String getTodaySpecial() {
		return todaySpecial;
	}
	public void setTodaySpecial(String todaySpecial) {
		this.todaySpecial = todaySpecial;
	}
	
}
