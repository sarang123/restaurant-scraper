package com.srapper.infra.spring;

import org.springframework.context.ApplicationContext;
import org.springframework.context.support.ClassPathXmlApplicationContext;

import play.Logger;

public final class SpringContext {

	private static final Object appContextLock = new Object();
	private static ClassPathXmlApplicationContext appContext;
	
	private SpringContext() {}
	
	public static ApplicationContext getApplicationContext() {
		synchronized (appContextLock) {
			try {
				if ( SpringContext.appContext == null ) {
					SpringContext.appContext = new ClassPathXmlApplicationContext("applicationContext.xml");
					Logger.debug("Spring applicationContext started");
				}
			} catch ( Exception e ) {
				Logger.error(e.getMessage());
			}		
		}
		return appContext;
	}
	
	public static void stopApplicationContext() {
		synchronized (appContextLock) {
			try {
				SpringContext.appContext.stop();
				SpringContext.appContext = null;
				Logger.debug("Spring applicationContext stopped");
			} catch ( Exception e ) {
				Logger.error(e.getMessage());
			}
		}	
	}
}
