package com.srapper.infra.repository;

import java.io.Serializable;

import org.springframework.data.repository.CrudRepository;

import com.srapper.app.domain.entities.RestaurantInfoEntity;



public interface RestaurantInfoRepository extends CrudRepository<RestaurantInfoEntity, Serializable> { 
	RestaurantInfoEntity findById(String id);
}
