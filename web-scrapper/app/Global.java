import com.srapper.infra.spring.SpringContext;

import play.Application;
import play.GlobalSettings;
import play.api.mvc.EssentialFilter;
import play.filters.gzip.GzipFilter;

public class Global extends GlobalSettings   {

	
	
	
	@Override
	public void onStart(Application app) {
		super.onStart(app); 
		SpringContext.getApplicationContext();
	     
	}

	@Override
	public void onStop(Application app) {
		SpringContext.stopApplicationContext();
		
	}
 	 
	@SuppressWarnings("unchecked")
	    public <T extends EssentialFilter> Class<T>[] filters() {
	        return new Class[] {
	        		GzipFilter.class
	        };
	    }
	
	
	
}
