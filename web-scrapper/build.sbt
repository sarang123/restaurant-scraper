name := """WebSrapper"""

version := "1.0-SNAPSHOT"

javacOptions ++= Seq("-source", "1.7", "-target", "1.7")

herokuJdkVersion in Compile := "1.7"

initialize := {
  val _ = initialize.value
  if (sys.props("java.specification.version") != "1.7")
    sys.error("Java 7 is required for this project.")
}

lazy val root = (project in file(".")).enablePlugins(PlayJava)

scalaVersion := "2.11.1"

scalacOptions in Compile += "-deprecation"

libraryDependencies ++= Seq(
  javaCore,
  javaJdbc,
  javaEbean,
  cache,
  "org.mongodb" % "mongo-java-driver" % "2.9.0-RC1",
  "org.quartz-scheduler" % "quartz" % "2.1.2",
  "org.slf4j" % "slf4j-api" % "1.6.6",
  "ch.qos.logback" % "logback-classic" % "1.0.7",
  "org.springframework" % "spring-context" % "3.2.9.RELEASE",
  "org.springframework" % "spring-tx" % "3.2.9.RELEASE",
  "org.springframework" % "spring-expression" % "3.2.9.RELEASE",
  "org.springframework" % "spring-aop" % "3.2.9.RELEASE",
  "org.springframework.data" % "spring-data-mongodb" % "1.5.0.RELEASE",
  "org.springframework.data" % "spring-data-commons" % "1.7.0.RELEASE",
  "joda-time" % "joda-time" % "2.3",
  "org.codehaus.groovy" % "groovy" % "2.0.0",
  "org.spockframework" % "spock-core" % "0.7-groovy-2.0",
  "javax.media" % "jmf" % "2.1.1e",
  "com.google.code.gson" % "gson" % "2.2.4",   
  filters,
  cache
)


