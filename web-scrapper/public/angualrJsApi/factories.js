'use strict';



/*  Factories */
var commertiaFactories = angular.module('scrapperApp.factory', ['ngResource']);

commertiaFactories.factory('webScrapperFactory', function ($resource, $http) {

       

        return $resource('/webScrapper/api/:action', {}, {

        	getRestaurantsFromUrl: {method: 'GET', params: {action: 'getRestaurantsFromUrl'}, isArray: true},
        	getRestaurantsFromDataBase: {method: 'GET', params: {action: 'getRestaurantsFromDataBase'}, isArray: true}
        });

    }
);

