﻿'use strict';

/* Controllers */
var controllers = angular.module('scrapperApp.controller', []);

/* Main Controller */
controllers.controller('ApplicationCtrl', [ 'webScrapperFactory', '$location',
		'$scope', function(webScrapperFactory, $location, $scope) {
		} ]);

controllers.controller('HomePageCtrl', [ 'webScrapperFactory', '$location',
		'$scope', function(webScrapperFactory, $location, $scope) {
			$scope.loading = false;
			
			$scope.doSearch = function(url) {
				if (url === "" || url == null || url == undefined) {
					alert("Please provide url");
					return false
				} else {
					if (url.search("zomato") == -1) {
						alert("Please provide url of zomato");
						return false
					}
				}

				$scope.restaturents = [];
				$scope.loading = true;
				webScrapperFactory.getRestaurantsFromUrl({
					"searchUrl" : url
				}, function(response) {
					console.log(response);
					$scope.restaturents = response;
					$scope.loading = false;
					if (!response[0])
						alert("No Result Found");
				}, function(e) {
					alert("Server have some problem . Please try again! ");
					console.log(e)
					$scope.loading = false;
					
				});
			}
			
			
			$scope.doGetDataBase = function() {
				

				$scope.restaturents = [];
				$scope.loading = true;
				webScrapperFactory.getRestaurantsFromDataBase( function(response) {
					console.log(response);
					$scope.restaturents = response;
					$scope.loading = false;
					if (!response[0])
						alert("No Result Found");
				}, function(e) {
					alert("Server have some problem . Please try again! ");
					console.log(e)
					$scope.loading = false;
					
				});
			}

		} ]);
