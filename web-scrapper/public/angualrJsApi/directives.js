'use strict';
/* Directives */
var directives = angular.module('scrapperApp.directive', []);

//loading and process
directives.directive('loading', function () {
    return {
        restrict: 'E',
        replace: true,
        template: '<div class="loadingRes" style="position:relative;margin-top:10%;"><div class="loaderAnimation"><span><img src="/images/commertiaLoader.gif" width="45" height="45" alt="Loader"> Updating results</span></div></div>',
        link: function (scope, element, attr) {
            scope.$watch('loading', function (val) {
                if (val)
                    $(element).show();
                else
                    $(element).hide();
            });
        }
    }
})







