﻿'use strict';

/** Initialize Modules **/

var app = angular.module(
    'scrapperApp', ['ui.router',
        'scrapperApp.controller',
        'scrapperApp.factory',
        'scrapperApp.directive'
    ]);

// This configures the routes and associates each route with a view and a controller
// http://stackoverflow.com/questions/27645202/what-is-the-difference-between-routeprovider-and-stateprovider-in-angularjs
// https://github.com/angular-ui/ui-router
app.config(['$stateProvider', '$urlRouterProvider', '$locationProvider', function ($stateProvider, $urlRouterProvider, $locationProvider) {
	$locationProvider.html5Mode({enabled: true,requireBase: false});
    $urlRouterProvider.otherwise("/");
    $stateProvider.state('index', {url: "/", templateUrl: "/views/home.html",controller: "HomePageCtrl"
    });    
}]);


