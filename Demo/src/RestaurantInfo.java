
public class RestaurantInfo {
	
	String name;
	String link;
	String area;
	String rating;
	String votes;
	String address;
    String cuisine;
    String type;
    String priceWithNopeople;
    String FeaturedInCollections;
    String offers;
    String timeForDelivery;
    String minAmount;
    String todaySpecial;
    
	public String getTodaySpecial() {
		return todaySpecial;
	}
	public void setTodaySpecial(String todaySpecial) {
		this.todaySpecial = todaySpecial;
	}
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public String getLink() {
		return link;
	}
	public void setLink(String link) {
		this.link = link;
	}
	public String getArea() {
		return area;
	}
	public void setArea(String area) {
		this.area = area;
	}
	public String getRating() {
		return rating;
	}
	public void setRating(String rating) {
		this.rating = rating;
	}
	public String getVotes() {
		return votes;
	}
	public void setVotes(String votes) {
		this.votes = votes;
	}
	public String getAddress() {
		return address;
	}
	public void setAddress(String address) {
		this.address = address;
	}
	public String getCuisine() {
		return cuisine;
	}
	public void setCuisine(String cuisine) {
		this.cuisine = cuisine;
	}
	public String getType() {
		return type;
	}
	public void setType(String type) {
		this.type = type;
	}
	public String getPriceWithNopeople() {
		return priceWithNopeople;
	}
	public void setPriceWithNopeople(String priceWithNopeople) {
		this.priceWithNopeople = priceWithNopeople;
	}
	public String getFeaturedInCollections() {
		return FeaturedInCollections;
	}
	public void setFeaturedInCollections(String featuredInCollections) {
		FeaturedInCollections = featuredInCollections;
	}
	public String getOffers() {
		return offers;
	}
	public void setOffers(String offers) {
		this.offers = offers;
	}
	public String getTimeForDelivery() {
		return timeForDelivery;
	}
	public void setTimeForDelivery(String timeForDelivery) {
		this.timeForDelivery = timeForDelivery;
	}
	public String getMinAmount() {
		return minAmount;
	}
	public void setMinAmount(String minAmount) {
		this.minAmount = minAmount;
	}
	@Override
	public String toString() {
		return "RestaurantInfo [name=" + name + ", link=" + link + ", area=" + area + ", rating=" + rating + ", votes="
				+ votes + ", address=" + address + ", cuisine=" + cuisine + ", type=" + type + ", priceWithNopeople="
				+ priceWithNopeople + ", FeaturedInCollections=" + FeaturedInCollections + ", offers=" + offers
				+ ", timeForDelivery=" + timeForDelivery + ", minAmount=" + minAmount + "]";
	}
    
    
}
