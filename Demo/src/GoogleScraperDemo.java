import java.io.IOException;
import java.util.ArrayList;

import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;
import org.jsoup.nodes.Element;
import org.jsoup.select.Elements;



public class GoogleScraperDemo {
	public static void main(String[] args) throws IOException {
int count = 1;
ArrayList<RestaurantInfo> restarants = new  ArrayList<>();
		for(int i=0;i<16;i++){
		
		String url = "https://www.zomato.com/mumbai/juhu-restaurants?page="+(count++);
		Document doc = Jsoup.connect(url).timeout(88888).get();
		Element link = doc.getElementById("orig-search-list");
		Elements lks = link.children();

		
		for (Element element : lks) {
			
			RestaurantInfo restarant = new RestaurantInfo();
			
			Elements restorrentName = element.select("a[class=result-title]");
			if(print(restorrentName)){
			String links =  restorrentName.get(0).attr("href");
			restarant.setName(restorrentName.get(0).text());
			restarant.setLink(links);
			}
			
			Elements areas = element.select("a[class=search-page-text zblack search_result_subzone]");
			if(print(areas))
				restarant.setArea(areas.get(0).text());
			
			Elements rating = element.select("div[class=tooltip rating-for-47221 res-rating-nf right level-7]");
			if(print(rating))
				restarant.setRating(rating.get(0).text());
			
			Elements noOfVotes = element.select("span[class=rating-votes-div-47221]");
			if(print(noOfVotes))
				restarant.setVotes(noOfVotes.get(0).text());
			
			Elements address = element.select("div[class=search-result-address zdark]");
			if(print(address))
				restarant.setAddress(address.get(0).text());
			
			Elements Cuisines = element.select("div[class=res-snippet-small-cuisine truncate search-page-text]");
			if(print(Cuisines))
				restarant.setCuisine(Cuisines.get(0).text());
			
			Elements types = element.select("a[class=cblack]");
			if(print(types))
				restarant.setType(types.get(0).text());
			
			Elements priceFor2 = element.select("div[class=res-cost]");
			if(print(priceFor2))
				restarant.setPriceWithNopeople(priceFor2.get(0).text());
			
			Elements Featured_in_Collections = element.select("div[class=srp-collections]");
			if(print(Featured_in_Collections))
				restarant.setFeaturedInCollections(Featured_in_Collections.get(0).text());
			
			Elements todaySpecial = element.select("div[class=search-result-events  mb10]");
			if(print(todaySpecial))
				restarant.setTodaySpecial(todaySpecial.get(0).text());
			
			Elements offers = element.select("div[class=search-result-offers]");
			if(print(offers))
				restarant.setOffers(offers.get(0).text());
			
			
			
			/*Elements timeForDelivery = element.select("div[class=zdark ttupper fontsize5]");
			if(print(timeForDelivery))
				restarant.setTimeForDelivery(timeForDelivery.get(1).parent().text());
			
			Elements minAmount = element.select("div[class=del-min-order]");
			if(print(minAmount))
				restarant.setMinAmount(minAmount.get(0).text());
			*/
			

			restarants.add(restarant);
			
		}
		
		

	}for (RestaurantInfo restaurantInfo : restarants) {
		System.out.println(restaurantInfo);
	}
		}

	public static boolean print(Elements elements) {
		if (elements != null && elements.size() > 0) {
            return true;
		}
		else
			return false;
	}
}
